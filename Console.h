#ifndef CONSOLE_H
#define CONSOLE_H

#include <iostream>
#include <mutex>
#include <string>

/* Defines synchronised write functions. Can accept any argument for printing
* The write functions behave similarly to the python3 print(Args...) function
*/
class Console {
private:
	static std::recursive_mutex _consoleMtx;

	template<typename T>
	static std::ostream& unpackToStream(std::ostream& ss, const T& t);

	template<typename T, typename... Args>
	static std::ostream& unpackToStream(std::ostream& ss, const T& t, const Args&... args);

public:
	template <typename T>
	static void write(const T& t);

	template <typename T, typename... Args>
	static void write(const T& t, const Args&... args);

	template <typename T>
	static void writeln(const T& t);

	template <typename T, typename... Args>
	static void writeln(const T& t, const Args&... args);
};

template <typename T>
void Console::write(const T& t) {
	Console::_consoleMtx.lock();
	std::cout << t;
	Console::_consoleMtx.unlock();
}

template <typename T, typename... Args>
void Console::write(const T& t, const Args&... args) {
	Console::_consoleMtx.lock();
	std::cout << t;
	unpackToStream(std::cout, args...);
	Console::_consoleMtx.unlock();
}

template <typename T>
void Console::writeln(const T& t) {
	Console::_consoleMtx.lock();
	std::cout << t << std::endl;
	Console::_consoleMtx.unlock();
}

template <typename T, typename... Args>
void Console::writeln(const T& t, const Args&... args) {
	Console::_consoleMtx.lock();
	std::cout << t;
	unpackToStream(std::cout, args...);
	std::cout << std::endl;
	Console::_consoleMtx.unlock();
}

template<typename T>
std::ostream& Console::unpackToStream(std::ostream& ss, const T& t) {
	ss << t;
	return ss;
}

template<typename T, typename... Args>
std::ostream& Console::unpackToStream(std::ostream& ss, const T& t, const Args&... args) {
	ss << t;
	return unpackToStream(ss, args...);
}

#endif