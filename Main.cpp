#include "Manager.h"
#include "Console.h"

// A program for testing io handling from 5 different sources
// The sources are simulated, and they send 3 numbers per packages (a,b,c)
// The manager sends a+b*c back to the sender

int main(int argc, char* argv[]) {
#define MakeSource std::make_shared<DataSource>
#define Source std::shared_ptr<DataSource>
#define Link(SRC) Manager::LinkSource(SRC);

	Source Ethernet = MakeSource(Sender::Ethernet);
	Source Wifi = MakeSource(Sender::Wifi);
	Source BT = MakeSource(Sender::Bluetooth);
	Source Connection4 = MakeSource(Sender::Connection4);
	Source Connection5 = MakeSource(Sender::Connection5);
	
	Console::writeln("Linking...");

	Link(Ethernet);
	Link(Wifi);
	Link(BT);
	Link(Connection4);
	Link(Connection5);

	Console::writeln("Starting manager ...");

	Manager::StartProcessing();

	Console::writeln("Activating generators");
	Console::writeln("Press enter to stop");

	Ethernet->StartGenerator();
	Wifi->StartGenerator();
	BT->StartGenerator();
	Connection4->StartGenerator();
	Connection5->StartGenerator();

	getchar();

	Console::writeln("Stopping ...");

	Ethernet->StopGenerator();
	Wifi->StopGenerator();
	BT->StopGenerator();
	Connection4->StopGenerator();
	Connection5->StopGenerator();
	Manager::StopProcessing();

	Console::writeln("Goodbye!");
	getchar();

#undef MakeSource
#undef Source
#undef Link
}
