#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <string>
#include <thread>
#include "Types.h"

typedef unsigned char byte;

enum class Sender : byte {
	Ethernet, Wifi, Bluetooth, Connection4, Connection5
};

std::string GetSenderName(Sender sender);

class DataSource {
public:
	DataSource(Sender);

	// Receive data from source
	void ReceiveData(Packet);
	
	// Send data back to source
	void SendReply(Reply);

	// Returns info about the sender of the data from this source
	const Sender& GetSender() const;



	// Starts the data source simulation
	void StartGenerator();

	// Stops the data source simulation
	void StopGenerator();

private:
	// A function that simulates a data source
	void GeneratorFunction();

	Sender _sender;

	std::thread _generatorThread;
	bool _generatorActive = false;
};

#endif