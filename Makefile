.PHONY: all clear
CC = g++
CFLAGS = -std=c++11 -pthread
OBJECTS = Console.o DataSource.o Manager.o Main.o

all: main

main: $(OBJECTS)
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clear:
	rm *.o