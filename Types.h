#ifndef TYPES_H
#define TYPES_H

struct Packet {
	float a;
	float b;
	float c;
	Packet(float A = 0, float B = 0, float C = 0) : a(A), b(B), c(C) {}
};

struct Reply {
	float result;
	Reply(float Result = 0) : result(Result) {}
};

#endif