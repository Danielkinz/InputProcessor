#include "DataSource.h"
#include "Manager.h"

#include <chrono>
#include <random>
#include "Console.h"

std::string GetSenderName(Sender sender) {
#define CASE(TYPE, RESULT) case TYPE: return RESULT

	switch (sender) {
		CASE(Sender::Ethernet, "Ethernet");
		CASE(Sender::Wifi, "Wifi");
		CASE(Sender::Bluetooth, "Bluetooth");
		CASE(Sender::Connection4, "Connection4");
		CASE(Sender::Connection5, "Connection5");
	}

	return "UnknownConnection";

#undef CASE
}

DataSource::DataSource(Sender sender) 
	: _sender(sender) {}

void DataSource::ReceiveData(Packet p) {
	Manager::AddRequest(_sender, p);
}

void DataSource::SendReply(Reply p) {
	Console::writeln("\t [", GetSenderName(_sender), "] Received reply from manager: ", p.result);
}

const Sender& DataSource::GetSender() const {
	return _sender;
}

void DataSource::GeneratorFunction() {
	std::chrono::system_clock::time_point next = std::chrono::system_clock::now();
	Packet p;
	// Random number generation
	std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_real_distribution<float> distribution(0.5, 5.0);

	while (_generatorActive) {
		if (next <= std::chrono::system_clock::now()) {
			// Creates packet
			p = Packet(distribution(generator), distribution(generator), distribution(generator));
			Console::writeln("[", GetSenderName(_sender), "] Creating request: (", p.a, ", ", p.b, ", ", p.c, "]");
			ReceiveData(p);

			// 0.5 - 5 seconds cooldown
			next = std::chrono::system_clock::now() + std::chrono::milliseconds(int(distribution(generator) * 1000));
		}
	}
}

void DataSource::StartGenerator() {
	_generatorActive = true;
	_generatorThread = std::thread(&DataSource::GeneratorFunction, this);
}

void DataSource::StopGenerator() {
	_generatorActive = false;
	_generatorThread.join();
}
