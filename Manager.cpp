#include "Manager.h"

std::queue<Manager::Request> Manager::_requests;
std::mutex Manager::_requestsQueueMutex;
std::condition_variable Manager::_requestsCV;
std::vector<std::shared_ptr<DataSource>> Manager::_sources;
bool Manager::_isRunning = false;
std::thread Manager::_processingThread;

std::shared_ptr<DataSource> Manager::FindSource(Sender sender) {
	for (auto it = _sources.begin(); it != _sources.end(); it++) {
		if ((*it)->GetSender() == sender) return *it;
	}
	return nullptr;
}

void Manager::LinkSource(std::shared_ptr<DataSource>& source) {
	if (!FindSource(source->GetSender())) {
		_sources.push_back(source);
	}
}

void Manager::AddRequest(Sender s, Packet p) {
	// Adds the request to the queue
	_requestsQueueMutex.lock();
	_requests.push(Request(s, p));
	_requestsQueueMutex.unlock();

	// Notifies the request processing thread
	_requestsCV.notify_one();
}

Reply Manager::Process(const Packet& p) {
	return Reply(p.a + p.b * p.c);
}

void Manager::RequestProcessor() {
	std::mutex mtx;
	Request r;
	
	while(_isRunning) {
		// Waits for a new request
		std::unique_lock<std::mutex> lk(mtx);
		_requestsCV.wait(lk);

		while (!_requests.empty()) {
			// Retrieves a request
			_requestsQueueMutex.lock();
			if (!_requests.empty()) {
				r = _requests.front();
				_requests.pop();
			}
			_requestsQueueMutex.unlock();

			// Processes the packet and returns the result
			FindSource(r.source)->SendReply(Process(r.data));
		}
	}
}

void Manager::StartProcessing() {
	_isRunning = true;
	_processingThread = std::thread(&Manager::RequestProcessor);
}

void Manager::StopProcessing() {
	_isRunning = false;
	_requestsCV.notify_all();
	_processingThread.join();
}
