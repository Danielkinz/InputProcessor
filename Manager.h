#ifndef MANAGER_H
#define MANAGER_H

#include <queue>
#include <memory>
#include <vector>
#include <mutex>
#include <condition_variable>
#include "DataSource.h"

class DataSource;

class Manager {
private:
	struct Request {
		Sender source;
		Packet data;
		Request(Sender s = Sender::Ethernet, Packet p = Packet()) : source(s), data(p) {}
	};

	// Sources

	static std::vector<std::shared_ptr<DataSource>> _sources;
	static std::shared_ptr<DataSource> FindSource(Sender);

	// Requests queue

	static std::queue<Request> _requests;
	static std::mutex _requestsQueueMutex;

	// Processing thread
	static Reply Process(const Packet&);

	static std::thread _processingThread;
	static std::condition_variable _requestsCV;
	static bool _isRunning;
	static void RequestProcessor();

public:
	static void LinkSource(std::shared_ptr<DataSource>&);
	static void AddRequest(Sender, Packet);

	static void StartProcessing();
	static void StopProcessing();
};

#endif